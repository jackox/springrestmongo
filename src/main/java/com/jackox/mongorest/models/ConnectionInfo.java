package com.jackox.mongorest.models;

class ConnectionInfo {
	private String hostInfo;
	private String portInfo;
	private String userID;
	private String password;
	private String dbName;

	public ConnectionInfo() {
	}

	public ConnectionInfo(String hostInfo, String portInfo, String userID, String password, String dbName) {
		super();
		this.hostInfo = hostInfo;
		this.portInfo = portInfo;
		this.userID = userID;
		this.password = password;
		this.dbName = dbName;
	}

	public String getPortInfo() {
		return portInfo;
	}

	public void setPortInfo(String portInfo) {
		this.portInfo = portInfo;
	}

	public String getHostInfo() {
		return hostInfo;
	}

	public void setHostInfo(String hostInfo) {
		this.hostInfo = hostInfo;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
}
