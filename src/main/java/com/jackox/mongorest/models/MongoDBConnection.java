package com.jackox.mongorest.models;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class MongoDBConnection extends ConnectionInfo {

	public MongoDBConnection(String hostInfo, String portInfo, String userID, String password, String dbName) {
		super(hostInfo, portInfo, userID, password, dbName);

	}

	MongoClientURI getURI() {

		String uriStr = "mongodb://";
		if (getUserID() != "" && getPassword() != "") {
			uriStr = uriStr + getUserID() + ":" + getPassword() + "@";
		}
		uriStr = uriStr + getHostInfo() + "/?authSource=" + getDbName();
		MongoClientURI uri = new MongoClientURI(uriStr);
		return uri;
	}

	public MongoClient getMongoClient() {
		return new MongoClient(getURI());
	}

	public MongoDatabase getDB(MongoClient mongoClient) {
		MongoDatabase database = mongoClient.getDatabase(getDbName());
		return database;
	}
	
	public MongoCollection<Document> selectCollection(MongoDatabase database, String collectionName) {
		return database.getCollection(collectionName);
	}
	

}
